import sys, random
from math import sqrt
import time

# Coord structure used to construct the distance matrix
class Coord:
  def __init__(self, i, x=0, y=0):
    self.index = i
    self.x = x
    self.y = y

  def __repr__(self):
    return "(" + str(self.index) + ", " + str(self.x) + ", " + str(self.y) + ")"
  def __str__(self):
    return "(" + str(self.index) + ", " + str(self.x) + ", " + str(self.y) + ")"

# Tour structure to hold chromosomes
class Tour:
  def __init__(self, tour):
    # List of integers representing city indexes in order
    self.tour = tour
    self.tour_length = len(tour)

  def __repr__(self):
    return str(self.tour)

  def __getitem__(self, key):
    return self.tour[key]

  # Swap the cities a and b
  def swap_cities_by_city(self, a, b):
    i1 = self.tour.index(a)
    i2 = self.tour.index(b)
    p1 = self.tour[i1]
    p2 = self.tour[i2]
    self.tour[i1] = p2
    self.tour[i2] = p1

  # Swap the cities at position a and b in the tour
  def swap_cities_by_index(self, a, b):
    p1 = self.tour[a]
    p2 = self.tour[b]
    self.tour[a] = p2
    self.tour[b] = p1


  def find_city_index(self, c):
    return self.tour.index(c)

  def get_slice(self, start, end):
    return self.tour[start:end]

  # Calculate TSP cost
  def get_noncvrp_cost(self, dist_mat):
    total = 0
    for j in range(1,self.tour_length):
      i = j-1
      p1,p2 = self.tour[i], self.tour[j]
      total += dist_mat[p1,p2]
    return total

  # Calculate cost of tour using a very crude method of optimising trucks
  # If the next city in the tour would overburden the current truck, move onto a new truck. Repeat until all cities have been processed
  def get_cost(self, dist_mat):
    # depot to first customer
    total = dist_mat[depot,self.tour[0]]
    current_cap = demands[self.tour[0]]
    for j in range(1, self.tour_length):
      i = j-1
      p1,p2 = self.tour[i], self.tour[j]
      new_cap = current_cap + demands[self.tour[j]]
      # Instead of i->j, i->depot->j
      # Add a new truck
      if new_cap > capacity:
        total += dist_mat[self.tour[i], depot]
        total += dist_mat[depot, self.tour[j]]
        current_cap = demands[self.tour[j]]
      else:
        dist =  dist_mat[p1,p2]
        total += dist
        current_cap = new_cap
    total += dist_mat[self.tour[-1],depot]
    return total


def dist(a,b):
  dx,dy = a.x - b.x, a.y - b.y
  d = sqrt(dx*dx + dy*dy)
  return d

def dist_matrix(coords):
  mat = {}
  for i,x in enumerate(coords):
    for j,y in enumerate(coords):
      mat[x.index,y.index] = dist(x,y)
  return mat


class GA:
  def __init__(self, dist_matrix, pop_size,  prob_crossover=0.9, prob_mutation=0.01, elitist=True, elitism = 0.15):
    self.prob_crossover = prob_crossover
    self.prob_mutation = prob_mutation
    self.dist_matrix = dist_matrix
    self.pop_size = pop_size
    self.population = None
    self.generation = 0
    self.elitism = elitism
    self.fitnesses = None
    self.total_f = 0
    self.elitist = elitist

  def termination_criteria(self, similar):
    if similar > 20 or self.generation > 1000: return True
    return False

  def run(self):
    similar = 0
    best = None
    self.population = self.initial_population()
    self.fitnesses, self.total_f = self.calc_fitnesses()
    best = min(self.fitnesses)
    while True:
      if min(self.fitnesses) >= best*0.9995: 
        similar += 1
      else: 
        similar = 0
      best = min(self.fitnesses)
      newpop = self.next_generation()
      self.population = newpop
      self.fitnesses, self.total_f = self.calc_fitnesses()
      if self.termination_criteria(similar): 
        break
    return self.population

  def calc_fitnesses(self):
    total_f = 0
    fs = map(self.fitness, self.population)
    total_f = sum(fs)
    return fs, total_f

  def next_generation(self):
    pop = []
    if self.elitist: pop = self.select_elite()
    # Go through the roulette wheel once, selecting enough parents to fill the rest of the new population
    parents = self.mass_roulette_selection(2*(self.pop_size - len(pop)))
    # Shuffle to remove adjacency bias (as we pick the parents from the wheel in ascending order, there is a bias towards adjacent indexes in the list being the same chromosome)
    random.shuffle(parents)
    # Pair up all the parents and create offspring
    for parent1, parent2 in zip(parents[::2], parents[1::2]):
      do_crossover = random.random() < self.prob_crossover
      if do_crossover:
        child = self.scx_crossover((parent1, parent2))
      else:
        child = parent1
      child = self.mutate(child)
      pop.append(child)
    self.generation += 1
    return pop

  def select_elite(self):
    elites = []
    num_to_select = int(self.pop_size*self.elitism+0.5)
    if num_to_select%2 == 1:
      num_to_select+= 1
    fs = zip(self.fitnesses, self.population)
    fs.sort()
    elites = [pair[1] for pair in fs[0:num_to_select]]
    return elites

  def initial_population(self):
    pop = []
    while len(pop) < self.pop_size:
      pop.append(Tour(random.sample(coords, len(coords))))
    return pop

  def fitness(self, chromosome):
    return chromosome.get_cost(self.dist_matrix)

  # Sequential Constructive Crossover
  # Aim is to maintain good edge sequence stucture from parents, while introducing new "good" edges where needed.
  def scx_crossover(self, parents):
    p1,p2 = parents
    length = p1.tour_length
    cities = coords
    cities_to_add = set(cities)
    # Choose a start city at random
    r = random.random()
    if r > 0.5:
      child = [p1.tour[0]]
    else:
      child = [p2.tour[0]]
    cities_to_add.remove(child[0])
    while bool(cities_to_add):
      current_node = child[-1]
      p1_index = p1.find_city_index(current_node)
      p2_index = p2.find_city_index(current_node)
      p1_node = None
      p2_node = None
      # Select next valid node from each parent 
      #for i in range(p1_index+1,length):
      for c in p1.tour[p1_index+1:length]:
        if c in cities_to_add: 
          p1_node = c
          break
      #for i in range(p2_index+1,length):
      for c in p2.tour[p2_index+1:length]:
        if c in cities_to_add: 
          p2_node = c
          break
      # If no valid nodes left in parent, choose first node from ordered set of cities (maybe better to choose random to prevent bias?) 
      if p1_node == None:
        p1_node = next(iter(cities_to_add))
      if p2_node == None:
        p2_node = next(iter(cities_to_add))
      # Take the node that has the lowest weight and add it to the child
      if dm[current_node, p1_node] < dm[current_node, p2_node]:
        child.append(p1_node)
        cities_to_add.remove(p1_node)
      else:
        child.append(p2_node)
        cities_to_add.remove(p2_node)
    return Tour(child)



  # Partially Matched Crossover
  def pmx_crossover(self, parents):
    p1,p2 = parents
    l = p1.tour_length
    cross_point = random.randint(0,l)
    child1 = Tour(list(p1.tour))
    child2 = Tour(list(p2.tour))
    for i in range(0, cross_point):
      # Swap p2[i] with c1[i], p1[i] with c2[i]
      child1.swap_cities_by_city(p2[i], child1.tour[i])
      child2.swap_cities_by_city(p1[i], child2.tour[i])
    return (child1, child2)


  # Standard swap mutation
  def mutate(self, chromosome):
    l = chromosome.tour_length
    for allele in range(0, l):
      do_mutate = random.random() < self.prob_mutation
      if do_mutate:
        i2 = random.randint(0, l-1)
        chromosome.swap_cities_by_index(allele,i2)
    return chromosome


  # Select num_to_pick parents via roulette wheel
  # Prevents iterating through self.fitnesses n times
  def mass_roulette_selection(self, num_to_pick):
    total = self.total_f
    randoms = [random.uniform(0,total) for i in range(num_to_pick)]
    randoms.sort(reverse=True)
    parents = []
    current = 0
    i = 0
    pick = randoms.pop()
    for f in self.fitnesses:
      current += f
      while current > pick:
        parents.append(self.population[i])
        try:
          pick = randoms.pop()
        except IndexError:
          return parents
      i += 1


  # Select a single parent via roulette wheel
  def roulette_selection(self):
    total = self.total_f
    pick    = random.uniform(0, total)
    current = 0
    i = 0
    for f in self.fitnesses:
      current += f
      if current > pick:
        return self.population[i]
      i += 1



def read_file():
  coords = []
  demands = {}
  global depot
  f = open('fruitybun250.vrp', 'r');

  line = f.readline()
  while line.find("CAPACITY") == -1:
    line = f.readline()
  c = line.split(" ")
  capacity = int(c[2])
  while line.find("NODE_COORD_SECTION") == -1:
    line = f.readline()

  # depot
  line = f.readline()
  xy = line.split(" ")
  depot = int(line[0])
  coords.append(Coord(int(xy[0]), int(xy[1]), int(xy[2])))
  i = 0
  while 1:
    line = f.readline()
    if line.find("DEMAND_SECTION") != -1: break
    xy = line.split(" ")
    coords.append(Coord(int(xy[0]), int(xy[1]), int(xy[2])))
  while 1:
    line = f.readline()
    if line.find("DEPOT_SECTION") != -1 or line == '': break
    d = line.split(" ")
    demands[int(d[0])] = int(d[1])
  f.close()
  return capacity, coords, demands

def write_solution(solution):
  cost = solution[0]
  tour = solution[1]
  print('login ct12755 53195')
  print('name Christopher Tyas')
  print('algorithm Genetic Algorithm using Sequential Constructive Crossover, standard swap mutation, and elitism')
  print('cost %.03f' % (cost))
  vehicle = [str(depot)]
  current_cap = 0
  for i in range(tour.tour_length):
    current_cap += demands[tour[i]]
    if current_cap <= capacity:
      vehicle.append(str(tour[i]))
    else:
      vehicle.append(str(depot))
      s = '->'.join(vehicle)
      print(s)
      vehicle = [str(depot),str(tour[i])]
      current_cap = demands[tour[i]]
  vehicle.append(str(depot))
  s = '->'.join(vehicle)
  print(s)


if __name__ == "__main__":
  global capacity
  global coords
  global demands
  global dm
  capacity, coords, demands = read_file()
  full_coords = coords
  dm = dist_matrix(coords)
  coords = range(depot+1,1+len(coords))
  ga = GA(dm,1500,1,0.007,True,0.15)
  end_pop = ga.run()
  coords = full_coords
  costs = []
  for tour in end_pop:
    cost = tour.get_cost(dm)
    costs.append((cost, tour))
  costs.sort()
  write_solution(costs[0])