from setuptools import setup, find_packages


with open('README.rst') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='CVRP',
    version='0.0.1',
    description='CVRP Assignment',
    long_description=readme,
    author='Chris Tyas',
    author_email='ct12755@my.bristol.ac.uk',
    url='',
    packages=find_packages(exclude=('tests', 'docs'))
)