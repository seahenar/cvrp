# tsp.py

import sys, random
from math import sqrt
import itertools
import time

PIL_SUPPORT = None

try:
   from PIL import Image, ImageDraw, ImageFont
   PIL_SUPPORT = True
except:
   PIL_SUPPORT = False

class Coord:
  newid = itertools.count().next
  def __init__(self, x=0, y=0):
    self.index = Coord.newid()
    self.x = x
    self.y = y

  def __repr__(self):
    return "(" + str(self.index) + ", " + str(self.x) + ", " + str(self.y) + ")"
  def __str__(self):
    return "(" + str(self.index) + ", " + str(self.x) + ", " + str(self.y) + ")"

class Tour:
  def __init__(self, tour):
    self.tour = tour
    self.tour_length = len(tour)

  def __repr__(self):
    return str(self.tour)

  def __getitem__(self, key):
    return self.tour[key]

  def swap_cities_by_index(self, a, b):
    p1 = self.tour[a]
    p2 = self.tour[b]
    self.tour[a] = p2
    self.tour[b] = p1

  def swap_cities_by_city(self, c1, c2):
    i1 = c1
    i2 = c2
    p1 = None
    p2 = None
    i = 0
    while (not p1 or not p2):
      if i >= self.tour_length:
        print("Error: city not found in tour")
        p1 = 0
        p2 = 0
        break
      p = self.tour[i]
      if p == i1: p1 = i
      if p == i2: p2 = i
      i += 1
    self.tour[p1] = c2
    self.tour[p2] = c1

  def find_city_index(self, c):
    index = 0
    for i,city in enumerate(self.tour):
      if city == c:
        index = i
        break
    return index

  def get_slice(self, start, end):
    return self.tour[start:end]

  def get_cost(self, dist_mat):
    total = 0
    for j in range(1,self.tour_length):
      i = j-1
      p1,p2 = self.tour[i], self.tour[j]
      total += dist_mat[p1,p2]
    return total


def dist(a,b):
  dx,dy = a.x - b.x, a.y - b.y
  d = sqrt(dx*dx + dy*dy)
  return d

def dist_matrix(coords):
  mat = {}
  for i,x in enumerate(coords):
    for j,y in enumerate(coords):
      mat[x.index,y.index] = dist(x,y)
  return mat

def write_coords_to_img(coords, img_file):
  """ The function to plot the graph """
  padding=20
  coords=[(c.x+padding,c.y+padding) for c in coords]
  maxx,maxy=0,0
  for x,y in coords:
    maxx=max(x,maxx)
    maxy=max(y,maxy)
    maxx+=padding
    maxy+=padding
  img=Image.new("RGB",(int(maxx),int(maxy)),color=(255,255,255))

  font=ImageFont.load_default()
  d=ImageDraw.Draw(img);
  for i,(x,y) in enumerate(coords):
    d.point((x,y), fill=(0,0,0))
    d.text((int(x)+7, int(y)-5),str(x-padding)+","+str(y-padding),fill=(32,32,32))

  del d
  img.save(img_file, "PNG")

  print "The plot was saved into the %s file." % (img_file,)

class GA:
  def __init__(self, dist_matrix, pop_size,  prob_crossover=0.9, prob_mutation=0.01, elitist=True, elitism = 0.15):
    self.prob_crossover = prob_crossover
    self.prob_mutation = prob_mutation
    self.dist_matrix = dist_matrix
    self.pop_size = pop_size
    self.population = None
    self.generation = 0
    self.elitism = elitism
    self.fitnesses = None
    self.wheel = None
    self.elitist = elitist

  def termination_criteria(self):
    if self.generation > 200: return True
    return False

  def run(self):
    self.population = self.initial_population()
    self.fitnesses = self.calc_fitnesses()
    while True:
      if self.generation%20 == 0:
        print(self.generation)
      newpop = self.next_generation()
      self.population = newpop
      self.fitnesses = self.calc_fitnesses()
      if self.termination_criteria(): break
    return self.population

  def calc_fitnesses(self):
    total_f = 0
    fs = []
    for i, chromosome in enumerate(self.population):
      f = self.fitness(chromosome)
      fs.append(f)
      total_f += f
    wheel = [0]
    for f in fs:
      wheel.append(wheel[-1]+(float(f)/total_f))
    self.wheel = wheel
    return fs

  def next_generation(self):
    pop = []
    if self.elitist: pop = self.select_elite()
    while len(pop) < self.pop_size:
      parent1 = self.roulette_selection()
      parent2 = self.roulette_selection()
      do_crossover = random.random() < self.prob_crossover
      if do_crossover:
        children = self.pmx_crossover((parent1,parent2))
      else:
        children = (parent1, parent2)
      child1 = self.mutate(children[0])
      child2 = self.mutate(children[1])
      pop.append(child1)
      pop.append(child2)
    self.generation += 1
    return pop

  def select_elite(self):
    elites = []
    num_to_select = int(self.pop_size*self.elitism+0.5)
    if num_to_select%2 == 1:
      num_to_select+= 1
    fs = zip(self.fitnesses, range(0,self.pop_size))
    fs.sort()
    for i in range(0,num_to_select):
      elites.append(self.population[fs[i][1]])
    return elites


  def initial_population(self):
    pop = []
    while len(pop) < self.pop_size:
      pop.append(Tour(random.sample(coords, len(coords))))
    return pop

  def fitness(self, chromosome):
    return chromosome.get_cost(self.dist_matrix)

  def pmx_crossover(self, parents):
    p1,p2 = parents
    l = p1.tour_length
    cross_point = random.randint(0,l)
    child1 = Tour(list(p1.tour))
    child2 = Tour(list(p2.tour))
    for i in range(0, cross_point):
      # Swap p2[i] with c1[i], p1[i] with c2[i]
      i1 = child1.find_city_index(p2[i])
      i2 = child2.find_city_index(p1[i])
      child1.swap_cities_by_index(i, i1)
      child2.swap_cities_by_index(i, i2)
    return (child1, child2)


  def mutate(self, chromosome):
    l = chromosome.tour_length
    for allele in range(0, l):
      do_mutate = random.random() < self.prob_mutation
      if do_mutate:
        i2 = random.randint(0,l-1)
        chromosome.swap_cities_by_index(allele,i2)
    return chromosome


  def roulette_selection(self, pop=None):
    if not pop: pop = self.population
    sel = random.random()
    i = 0
    while(sel>self.wheel[i+1]):
      i += 1
    return pop[i]

def generate_random_coords(num, maxwidth, maxheight):
  coords = []
  while len(coords) < num:
    w = random.randint(0,maxwidth)
    h = random.randint(0,maxheight)
    c = Coord(w,h)
    coords.append(c)
  return coords


BAYS29 = [Coord(1150,1760),Coord(630,1660),Coord(40,2090),Coord(750,1100),Coord(750,2030),Coord(1030,2070),Coord(1650, 650),Coord(1490,1630),Coord(790,2260),Coord(710,1310),Coord(840, 550),Coord(1170,2300),Coord(970,1340),Coord(510, 700),Coord(750, 900),Coord(1280,1200),Coord(230, 590),Coord(460, 860),Coord(1040, 950),Coord(590,1390),Coord(830,1770),Coord(490, 500),Coord(1840,1240),Coord(1260,1500),Coord(1280, 790),Coord(490,2130),Coord(1460,1420),Coord(1260,1910),Coord(360,1980)]


def mk_BAYS29_matrix():
  """Compute a distance matrix for a set of points.

  Uses function 'dist' to calculate distance between
  any two points.  Parameters:
  -coord -- list of tuples with coordinates of all points, [(x1,y1),...,(xn,yn)]
  -dist -- distance function
  """
  D = {}      # dictionary to hold n times n matrix
  f = open('bays29.tsp', 'r');

  line = f.readline()
  while line.find("EDGE_WEIGHT_SECTION") == -1:
    line = f.readline()

  i = 0
  while 1:
    line = f.readline()
    if line.find("DISPLAY_DATA_SECTION") != -1: break
    dists = line.split()
    for j in range(0, len(dists)):
      D[i,j] = int(dists[j])
    i += 1
  return D

if __name__ == "__main__":
  global coords
  global dm
  print("Starting test")
  #coords = generate_random_coords(10,30,40)
  coords = BAYS29
  #n, xy_pos, dm, coords = read_tsplib('berlin52.tsp')
  #dm = dist_matrix(coords)
  dm = mk_BAYS29_matrix()
  start = time.clock()
  coords = range(len(BAYS29))
  ga = GA(dm,1000,1,0.007,True,0.15)
  end_pop = ga.run()
  coords = BAYS29
  print("GA finished")
  end = time.clock()
  print(end-start)
  costs = []
  for tour in end_pop:
    cost = tour.get_cost(dm)
    costs.append((cost, tour))
  costs.sort()
  print("best = " + str(costs[0]))
  print("worst = " + str(costs[-1]))
  write_coords_to_img(coords, "coords.png")

